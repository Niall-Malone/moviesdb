﻿using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MoviesDB.Controllers;
using MoviesDB.Models;
using System.Diagnostics;

namespace MoviesDBTest
{
    [TestClass]
    public class UnitTest1
    {

        private MoviesEntities3 _db = new MoviesEntities3();

        [TestMethod]
        public void TestDetails()
        {
            var controller = new HomeController();
            var result = controller.testDetails(1) as ViewResult;
            Assert.AreEqual("Details", result.ViewName);
        }


        [TestMethod]
        public void TestMovieDetails()
        {
            var controller = new HomeController();
            var result = controller.testMovie(1) as ViewResult;
            var mov = (Movie)result.ViewData.Model;
            //System.Console.Write(result);
            //System.Console.Write(mov);
            //Debug.WriteLine(mov);
            Assert.AreEqual("Memento", mov.Title);
        }

        [TestMethod]
        public void TestCreateMovie()
        {
            var controller = new HomeController();
            var result = controller.testCreateMovie(1, "Matrix", "Wachoski Bros") as ViewResult;
            var mov = (Movie)result.ViewData.Model;
            //System.Console.Write(result);
            //System.Console.Write(mov);
            //Debug.WriteLine(mov);
            Assert.AreEqual("Matrix", mov.Title);
        }

    }
}
