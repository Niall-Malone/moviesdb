﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Logon.aspx.cs" Inherits="MoviesDB.Logon" %>
<%@ Import Namespace="System.Web.Security" %>

<script runat="server">
  void Logon_Click(object sender, EventArgs e)
  {
      if (UserEmail.Text.IndexOf("@") > -1)
      {
          if ((UserEmail.Text == "niall@me.com") &&
                    (UserPass.Text == "liv123pool"))
          {
              FormsAuthentication.RedirectFromLoginPage(UserEmail.Text, Persist.Checked);
          }
          else
          {
              Msg.Text = "Invalid credentials. Please try again.";
          }
      }
      else
      {
          Msg.Text = "You must use your email address.";
      }
            
  }
</script>
<html>
<head id="Head1" runat="server">
  <title>Login</title>
</head>
<body>
  <form id="form1" runat="server">
    <div style="position: relative; margin-left: auto; margin-right: auto; border-style: ridge; border-width: 10px; width: 50%; margin-top: 20%;">
            <h3 style="margin-left: 40%;">Logon Page :</h3>
            <table>
              <tr>
                <td>
                  <label style="margin-left: 40px;">E-mail address:</label></td>
                <td>
                  <asp:TextBox ID="UserEmail" runat="server" /></td>
                <td>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator1" 
                    ControlToValidate="UserEmail"
                    Display="Dynamic" 
                    ErrorMessage="Cannot be empty." 
                    runat="server" />
                </td>
              </tr>
              <tr>
                <td><label style="margin-left: 40px">Password:</label></td>
                <td>
                  <asp:TextBox ID="UserPass" TextMode="Password" 
                     runat="server" />
                </td>
                <td>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator2" 
                    ControlToValidate="UserPass"
                    ErrorMessage="Cannot be empty." 
                    runat="server" />
                </td>
              </tr>
              <tr>
                <td><label style="margin-left: 40px;">Remember me?</label></td>
                <td>
                  <asp:CheckBox ID="Persist" runat="server" /></td>
              </tr>
            </table>
            <asp:Button ID="Submit1" OnClick="Logon_Click" Text="Log On" 
               runat="server" />
            <p>
              <asp:Label ID="Msg" ForeColor="red" runat="server" style="margin-left: 10px;"/>
            </p>

            <style>
                #Submit1 { margin-left: 40px;}
                #UserEmail { width: 300px;}
                #UserPass { width: 300px;}
            </style>
    </div>
  </form>
</body>
</html>
