﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Diagnostics;
using MoviesDB.Models;
using System.Data.Entity.Infrastructure;

namespace MoviesDB.Controllers
{


    public class HomeController : Controller
    {
        
        private MoviesEntities3 _db = new MoviesEntities3();

        public ActionResult Index()
        {
            ViewBag.Message = "Movie Database editor :";
            ViewBag.Operation = "Sort A -> Z";
            ViewBag.Function = "ASC";
            return View(_db.Movies.ToList());
        }

        // GET : /HOME/ASC

        public ActionResult ASC()
        {
            ViewBag.Message = "Movie Database editor :";
            ViewBag.Operation = "Sort Z -> A";
            ViewBag.Function = "DSC";
            List<Movie> sList = _db.Movies.ToList();
            sList = sList.OrderBy(o => o.Title).ToList();
            //sList = sList.OrderByDescending(o => o.Title).ToList();
            return View(sList);
        }

        // GET : /HOME/ASC

        public ActionResult DSC()
        {
            ViewBag.Message = "Movie Database editor :";
            ViewBag.Operation = "Sort A -> Z";
            ViewBag.Function = "ASC";
            List<Movie> sList = _db.Movies.ToList();
            //sList = sList.OrderBy(o => o.Title).ToList();
            sList = sList.OrderByDescending(o => o.Title).ToList();
            return View(sList);
        }

        // GET: /Home/Create 

        public ActionResult Create()
        {
            ViewBag.Message = "Create your movie :";
            return View();
        }

        //

        // POST: /Home/Create 

        [AcceptVerbs(HttpVerbs.Post)]

        public ActionResult Create([Bind(Exclude = "Id")] Movie movieToCreate)
        {

            if (ModelState.IsValid)
            {

                //return View();

                _db.Movies.Add(movieToCreate);
                _db.SaveChanges();

            }

            return RedirectToAction("Index");

        }

        public ActionResult FileUpload(HttpPostedFileBase file, String nom)
        {
            if (file != null)
            {
                
                string pic = nom + ".jpg";
                //System.IO.Path.GetFileName(file.FileName);
                string path = System.IO.Path.Combine(
                                       Server.MapPath("/Content/Images/"), pic);
                // file is uploaded
                file.SaveAs(path);

            }
            // after successfully uploading redirect the user
            return RedirectToAction("Index", "Home");
        }

        // GET: /Home/Edit/5 

        public ActionResult Edit(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(404);
            }

            Movie mov = _db.Movies.Single(m => m.Id == id);
            if (mov == null)
            {
                return new HttpStatusCodeResult(404);
            }

            ViewBag.Message = "Currently editing : " + mov.Title;
            return View(mov);

        }

        //

        // POST: /Home/Edit/5 

        [AcceptVerbs(HttpVerbs.Post)]

        public ActionResult Edit(Movie movieToEdit)
        {

            var originalMovie = (from m in _db.Movies 
                                 where m.Id == movieToEdit.Id 
                                 select m).First();

            if (TryUpdateModel(originalMovie, "",
               new string[] { "Title", "Director", "DateReleased" }))
            {
                try
                {
                    _db.SaveChanges();

                    return RedirectToAction("Index");
                }
                catch (RetryLimitExceededException /* dex */)
                {
                    //Log the error (uncomment dex variable name and add a line here to write a log.
                    ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists, see your system administrator.");
                }
            }   
                
            return RedirectToAction("Index");

        }


        // GET: /Home/Delete/5 
        public ActionResult Delete(int id, bool? saveChangesError = false)
        {
            
            if (id == null)
            {
                return new HttpStatusCodeResult(404);
            }

            Movie mov = _db.Movies.Single(m => m.Id == id);
            if (mov == null)
            {
                return new HttpStatusCodeResult(404);
            }

            ViewBag.Message = "Currently deleting : " + mov.Title;
            return View(mov);

        }

        // POST: /Home/Delete/5 

        [AcceptVerbs(HttpVerbs.Post)]

        public ActionResult Delete(int id)
        {

            try
            {
                Movie mov = _db.Movies.Find(id);
                _db.Movies.Remove(mov);
                _db.SaveChanges();
            }
            catch (RetryLimitExceededException/* dex */)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                return RedirectToAction("Delete", new { id = id, saveChangesError = true });
            }

            return RedirectToAction("Index");

        }


        // GET: /Home/Details/5 
        public ActionResult Details(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(404);
            }

            Movie mov = _db.Movies.Single(m => m.Id == id);
            ViewBag.Message = mov.Title + " Details :";
            if (mov == null)
            {
                return new HttpStatusCodeResult(404);
            }

            return View(mov);

        }

        //=========================TEST METHODS=========================

        public ActionResult testDetails(int id)
        {
            return View("Details");
        }

        public ActionResult testMovie(int id)
        {
            var mov = new Movie(id,"Memento","Lucas");
            return View("testMovie", mov);
        }

        public ActionResult testCreateMovie(int id,String t,String d)
        {
            var mov = new Movie(id, t, d);
            return View("testCreateMovie", mov);
        }

    }

} 
